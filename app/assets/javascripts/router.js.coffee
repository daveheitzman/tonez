# For more information see: http://emberjs.com/guides/routing/

Tonez.Router.map ()->
  @resource('things')
  @resource 'molecules', ( ()->
    @route("new", {path: 'new'})
    @route("edit", {path: '/:molecule_id/edit'})
  )

Tonez.ThingRoute=Ember.Route.extend(
  model: ()->
    @store.find('thing')
)

Tonez.MoleculesEditRoute=Ember.Route.extend(
  model: (params)->
    window.console.log 'MoleculesEditRoute'
    window.console.log params
    # {molecule: {id:1, molecular_weight:23, name, created_at: '2013-08-09', updated_at:'2010-12-11' , atoms: ['23']} , atoms: [ {id: 23}] }
    @store.find('molecule', params.molecule_id )
)

Tonez.MoleculesIndexRoute=Ember.Route.extend(
  model: (params)->
    window.console.log('molecules_index_route')
    @store.find('molecule').then( (promise,d)->
      window.console.log 'd'
      window.console.log d
      window.console.log  'promise' 
      window.console.log  promise 
      window.console.log  'promise.molecules' 
      window.console.log  promise.molecules    
      promise 
    )

  setupController: (model, params)->
    window.console.log 'setupController: (model, params)->'
    window.console.log model
    window.console.log params
    @set 'atoms', model.atoms
    @set 'model', model
)

Tonez.MoleculesRoute=Ember.Route.extend(
  model: (params)->
    @store.find('molecule').then( (promise,d)->
      window.console.log  'promise' 
      window.console.log  promise 
      window.console.log 'd'
      window.console.log d
      window.console.log  'promise.atoms' 
      window.console.log  promise.atoms 
      promise 
    )
    # Ember.Object.create {"molecules":[ {"id":1,"molecular_weight":"781.0","name":"zeluorothoriumine","created_at":"2013-12-26T15:05:49.436Z","updated_at":"2013-12-26T15:05:49.436Z","atoms":[1,2,3,4]}] , atoms: [ {id:1} ,{id:2},{id:3},{id:4} ] }
    # Ember.Object.create {"molecules":[ {"id":1,"molecular_weight":"781.0","name":"zeluorothoriumine","created_at":"2013-12-26T15:05:49.436Z","updated_at":"2013-12-26T15:05:49.436Z","atoms":[1,2,3,4]}]  }

    # {"molecules":[{"id":1,"molecular_weight":"781.0","name":"fluorothoriumine","created_at":"2013-12-26T15:05:49.436Z","updated_at":"2013-12-26T15:05:49.436Z","atoms":[1,2,3,4]}] }
    # this is what finally worked 
    # {
      # "atoms":[{"id":1,"molecular_weight":671,"name":"helium","created_at":"2013-12-26T15:05:48.614Z","updated_at":"2013-12-26T15:05:49.610Z","molecule_id":1},{"id":2,"molecular_weight":234,"name":"thorium","created_at":"2013-12-26T15:05:48.758Z","updated_at":"2013-12-26T15:05:49.612Z","molecule_id":1},{"id":3,"molecular_weight":11,"name":"fluorine","created_at":"2013-12-26T15:05:48.899Z","updated_at":"2013-12-26T15:05:49.613Z","molecule_id":1},{"id":4,"molecular_weight":189,"name":"spazium","created_at":"2013-12-26T15:05:49.041Z","updated_at":"2013-12-26T15:05:49.614Z","molecule_id":1}], 
      # "molecules":[{"id":1,"molecular_weight":"781.0","name":"fluorothoriumine","created_at":"2013-12-26T15:05:49.436Z","updated_at":"2013-12-26T15:05:49.436Z","atom_ids":[1,2,3,4]}]}
)
