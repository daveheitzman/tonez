# for more details see: http://emberjs.com/guides/views/

Tonez.MoleculesView = Ember.View.extend
  templateName: 'molecules/index'

Tonez.MoleculesIndexView = Ember.View.extend
  templateName: 'molecules/index'

Tonez.MoleculeView = Ember.View.extend
  templateName: 'molecules/edit'

Tonez.MoleculesEditView = Ember.View.extend
  templateName: 'molecules/edit'
