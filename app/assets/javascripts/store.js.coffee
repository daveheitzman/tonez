# http://emberjs.com/guides/models/using-the-store/

Tonez.Store = DS.Store.extend
  adapter: DS.ActiveModelAdapter
  # Override the default adapter with the `DS.ActiveModelAdapter` which
  # is built to work nicely with the ActiveModel::Serializers gem.
  # adapter: '_ams'

Tonez.Store.reopen()

# FwM.Store = DS.Store.extend
#   adapter: DS.ActiveModelAdapter

# # set the prefix it will use on api calls
# DS.ActiveModelAdapter.reopen
#   namespace: 'api/v1'


