#= require jquery
#= require jquery_ujs
#= require handlebars
#= require ember
#= require ember-data
#= require_self
#= require tonez

# for more details see: http://emberjs.com/guides/application/
window.Tonez = Ember.Application.create(
  LOG_TRANSITIONS: true,
  LOG_BINDINGS: true,
  LOG_VIEW_LOOKUPS: true,
  LOG_STACKTRACE_ON_DEPRECATION: true,
  LOG_VERSION: true,
  debugMode: true,
  LOG_TRANSITIONS_INTERNAL: true # detailed logging of all routing steps
)

