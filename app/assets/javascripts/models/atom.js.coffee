# for more details see: http://emberjs.com/guides/models/defining-models/

Tonez.Atom = DS.Model.extend(
  name: DS.attr("string")
  molecular_weight: DS.attr("number")
  created_at: DS.attr("date")
  updated_at: DS.attr("date")
  molecule: DS.belongsTo("molecule", { inverse: 'atoms' } )
)
