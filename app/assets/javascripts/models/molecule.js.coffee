# for more details see: http://emberjs.com/guides/models/defining-models/

Tonez.Molecule = DS.Model.extend(
  name: DS.attr("string")
  molecular_weight: DS.attr("number")
  created_at: DS.attr("date")
  updated_at: DS.attr("date")
  atoms: DS.hasMany("atom", {inverse: 'molecule' } )
)
