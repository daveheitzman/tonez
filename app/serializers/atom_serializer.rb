class AtomSerializer < ActiveModel::Serializer
  attributes :id, :molecular_weight, :name, :created_at, :updated_at , :molecule_id
end
