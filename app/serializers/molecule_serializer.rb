class MoleculeSerializer < ActiveModel::Serializer
  attributes :id, :molecular_weight, :name, :created_at, :updated_at # , :atoms
  has_many :atoms
  embed :ids, :include=>true
  # def atoms
  #   object.atoms.map(&:id)
  # end 

end
