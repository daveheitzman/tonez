class MoleculesController < ApplicationController
  before_action :set_molecule, only: [:show, :edit, :update, :destroy]

  # GET /molecules
  # GET /molecules.json
  def index
    @molecules = Molecule.all
    respond_to do |format|
      format.html { redirect_to @molecule, notice: 'Molecule was successfully created.' }
      # format.json { render :json=>{ :molecules=>@molecules, :atoms=>Atom.all.to_a },  status: 200 }
      format.json { render :json=> @molecules ,  status: 200 }
      # format.json { render :text=>%Q| {"molecules":[{"id":1,"molecular_weight":"781.0","name":"textothoriumine","created_at":"2013-12-26T15:05:49.436Z","updated_at":"2013-12-26T15:05:49.436Z", "atoms":[26] }] , "atoms":[{"id": 26} ] } | ,  status: 200 }
    end
    
  end

  # GET /molecules/1
  # GET /molecules/1.json
  def show
    edit
    return
  end

  # GET /molecules/new
  def new
    @molecule = Molecule.new
  end

  # GET /molecules/1/edit
  def edit
    # format.json { render :json=>{ :molecule=>@molecule, :atoms=>@molecules.atoms },  status: 200 }
    @molecule=Molecule.find params[:id] 
    respond_to do |format|
      format.json { render :json=> @molecule ,  status: 200 }
    end
  end

  # POST /molecules
  # POST /molecules.json
  def create
    @molecule = Molecule.new(molecule_params)

    respond_to do |format|
      if @molecule.save
        format.html { redirect_to @molecule, notice: 'Molecule was successfully created.' }
        format.json { render action: 'show', status: :created, location: @molecule }
      else
        format.html { render action: 'new' }
        format.json { render json: @molecule.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /molecules/1
  # PATCH/PUT /molecules/1.json
  def update
    respond_to do |format|
      if @molecule.update(molecule_params)
        format.html { redirect_to @molecule, notice: 'Molecule was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @molecule.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /molecules/1
  # DELETE /molecules/1.json
  def destroy
    @molecule.destroy
    respond_to do |format|
      format.html { redirect_to molecules_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_molecule
      @molecule = Molecule.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def molecule_params
      params[:molecule]
    end
end
