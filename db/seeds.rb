# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)




Parent.delete_all
Parent.create :name=>"parent of many things", :last_employee_update=>5.days.ago
Thing.delete_all
Thing.create :name=>'rope', :size=>26, :parent_id=>Parent.first
Thing.create :name=>'furry creature', :size=>11, :parent_id=>Parent.first

Atom.delete_all 
Atom.create :name=>'helium' , :molecular_weight=> 671 
Atom.create :name=>'thorium' , :molecular_weight=> 234 
Atom.create :name=>'fluorine' , :molecular_weight=> 11
Atom.create :name=>'spazium' , :molecular_weight=> 189

Molecule.delete_all
m=Molecule.create :name => 'fluorothoriumine', :molecular_weight=>781
m.atoms += Atom.all.to_a
m.save 


