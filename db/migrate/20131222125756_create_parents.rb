class CreateParents < ActiveRecord::Migration
  def change
    create_table :parents do |t|
      t.string :name 
      t.datetime :last_employee_update 
      t.timestamps
    end
  end
end
