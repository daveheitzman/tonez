class CreateAtoms < ActiveRecord::Migration
  def change
    create_table :atoms do |t|
      t.string :name 
      t.integer :molecular_weight
      t.integer :molecule_id
      t.timestamps
    end
  end
end
