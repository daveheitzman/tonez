class CreateMolecules < ActiveRecord::Migration
  def change
    create_table :molecules do |t|
      t.string :name 
      t.decimal :molecular_weight, :precision=>18, :scale=>2
      t.timestamps
    end
  end
end
